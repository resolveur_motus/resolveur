import importlib.resources as importres
from collections import defaultdict
from math import exp
from typing import DefaultDict, List, Optional
import unicodedata


def normalize_word(word: str) -> str:
    return "".join(c.lower() for c in unicodedata.normalize("NFD", word) if unicodedata.category(c) != "Mn" and c.isalpha())

def sigmoid(x: float) -> float:
    return 1 / (1 + exp(-x))

class Dictionary:
    def __init__(self, path: str, freq_path: Optional[str] = None) -> None:
        with importres.open_text('assets', path, encoding='utf-8') as file:
            lines = file.readlines()[2:]
        
        self.word_frequencies = None

        if freq_path:
            self.word_frequencies = {}
            with importres.open_text('assets', freq_path, encoding='utf-8') as file:
                for row in file.read().splitlines()[1:]:
                    tokens = row.split(",")
                    self.word_frequencies[normalize_word(tokens[0])] = float(tokens[2])
                max_freq = max(self.word_frequencies.values())
                for word in self.word_frequencies.keys():
                    self.word_frequencies[word] /= max_freq

        words_generator = (normalize_word(word.split("/")[0]) for word in lines)
        words = filter(lambda word: len(word) > 0 and word[0].islower(), words_generator)
        self.words = list(set(words))

        self.minimum_counts = defaultdict(lambda: 0)

        self.was_length_filtered = False
    
    def score_word(self, word: str) -> float:
        if self.word_frequencies is not None:
            word_freq_score = sigmoid(self.word_frequencies.get(word, 0))
        else:
            word_freq_score = 0.0
        
        return word_freq_score

    def filter_by_length(self, length: int) -> None:
        self.words = list(filter(lambda word: len(word) == length, self.words))
        if self.word_frequencies is not None:
            self.word_frequencies = {word: freq for word, freq in self.word_frequencies.items() if len(word) == length}
        self.was_length_filtered = True
    
    def filter_with_pattern(self, guess: str, pattern: str) -> None:
        """
        pattern - str of length equal to guess, each character should be one of
        - 'o': the letter is in the right place
        - '-': the letter is incorrect
        - '~': the letter appears elsewhere in the word but is not in the right place
        """

        assert(len(guess) == len(pattern))
        assert(len(guess) > 0)

        if not self.was_length_filtered:
            self.filter_by_length(len(guess))

        new_minimum_counts = defaultdict(lambda: 0)

        filters = []

        for (i, (letter, state)) in enumerate(zip(guess, pattern)):
            if state == 'o':
                new_minimum_counts[letter] += 1
                filters.append(lambda word, i=i, letter=letter: word[i] == letter)
            if state == '~':
                new_minimum_counts[letter] += 1
                filters.append(lambda word, i=i, letter=letter: word[i] != letter)

        for letter, count in new_minimum_counts.items():
            self.minimum_counts[letter] = max(count, self.minimum_counts[letter])

        for (i, (letter, state)) in enumerate(zip(guess, pattern)):
            if state == '-':
                filters.append(lambda word, letter=letter: word.count(letter) == self.minimum_counts[letter])
        
        for letter in self.minimum_counts.keys():
            filters.append(lambda word, letter=letter: word.count(letter) >= self.minimum_counts[letter])

        self.words = list(filter(lambda word: all(f(word) for f in filters), self.words))
        self.words.sort(key=self.score_word, reverse=True)

def get_pattern_for_word(guess: str, to_guess: str) -> str:
    """
    Returns the pattern (as in Dictionary.filter_with_pattern) when the correct word is known.
    """

    ret = ""

    misplaced_letters = {}
    for guessed_letter, letter in zip(guess, to_guess):
        if guessed_letter != letter:
            misplaced_letters[letter] = misplaced_letters.get(letter, 0) + 1

    for guessed_letter, letter in zip(guess, to_guess):
        if guessed_letter == letter:
            ret += "o"
        else:
            if misplaced_letters.get(guessed_letter, 0) > 0:
                ret += "~"
                misplaced_letters[guessed_letter] -= 1
            else:
                ret += "-"

    return ret


if __name__ == "__main__":
    import tui
    tui.wordeule_tui()