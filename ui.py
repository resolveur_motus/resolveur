import string
from time import sleep
from typing import List, cast
from enum import Enum
from PySide6.QtWidgets import *
from PySide6 import QtCore
import wordeule

class LetterState(Enum):
	INCORRECT = 0
	CORRECT = 1
	MISPLACED = 2

class LetterButton(QPushButton):
	def __init__(self):
		super().__init__()
		self.state = LetterState.INCORRECT

	def widthForHeight(self, width: int) -> int:
		return width

class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()

		self.setWindowTitle("Wordeule™")
		self.setGeometry(100, 100, 600, 400)

		self.known_dictionaries = {
			"Anglais": {"path": "en_US.dic", "freq_path": "en_US.freq.csv"},
			"Français": {"path": "fr_FR.dic"}
		}

		# Main screen
		self.intro = QLabel("""\
Bienvenue sur le solveur Wordeule™!
Ce solveur peut vous aider à résoudre des parties de Motus, de Wordle, ou d'autres jeux similaires.
""")

		self.label_language = QLabel("Commencez par choisir votre langue:")
		self.language_select = QComboBox()
		for entry in self.known_dictionaries.keys():
			self.language_select.addItem(entry)

		self.label_first_letter = QLabel("Si vous connaissez la première lettre, renseignez-la:")
		self.first_letter_select = QComboBox()
		self.first_letter_select.addItem("")
		for letter in string.ascii_uppercase:
			self.first_letter_select.addItem(letter)

		self.label_start = QLabel("Choisissez le nombre de lettres du mot à deviner:")

		self.start_buttons = []
		for i in range(4, 10):
			new_button = QPushButton()
			new_button.setText(f"{i}")
			new_button.clicked.connect((lambda i: lambda: self.open_solver_from_main_params(i))(i))
			self.start_buttons.append(new_button)
		
		self.label_explanation = QLabel("Veuillez saisir le motif des lettres obtenu:")
		self.label_carousel = QLabel("Cliquez sur les cases pour faire varier les couleurs.")

		self.label_colors = QLabel("""\
VERT - La lettre à l'emplacement spécifique est correctement placée
JAUNE - La lettre à l'emplacement spécifique n'est pas correctement placée mais apparait dans le mot
GRIS - La lettre à l'emplacement spécifique n'apparait pas dans le mot
""")
		
		self.word_select_label = QLabel("Autres propositions:")
		self.word_select = QComboBox()
		self.word_select.currentTextChanged.connect(lambda word: self.set_primary_proposition(word))
		self.button_validate = QPushButton("Valider")
		
		self.letter_buttons = []
		for i in range(9):
			new_button = LetterButton()
			new_button.clicked.connect(self.cycle_letter_button)
			new_button.setFont("Courier New")
			self.letter_buttons.append(new_button)
		
		self.startup = QVBoxLayout()
		self.startup.addWidget(self.intro)
		self.startup.addWidget(self.label_language)
		self.startup.addWidget(self.language_select)
		self.startup.addWidget(self.label_first_letter)
		self.startup.addWidget(self.first_letter_select)
		self.startup.setContentsMargins(16, 16, 16, 16)
		self.startup.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)

		self.startup.addWidget(self.label_start)

		self.start_button_layout = QHBoxLayout()
		for start_button in self.start_buttons:
			self.start_button_layout.addWidget(start_button)
		self.startup.addLayout(self.start_button_layout)

		self.main_menu_widget = QWidget()
		self.main_menu_widget.setLayout(self.startup)
		
		self.button_validate.clicked.connect(self.validate_word)

		# Solver screen
		self.solver_screen = QVBoxLayout()
		self.solver_screen.setAlignment(QtCore.Qt.AlignVCenter | QtCore.Qt.AlignHCenter)
		
		self.proposition_layout = QHBoxLayout()
		self.spots = QHBoxLayout()

		alt_propositions = QVBoxLayout()
		alt_propositions.addWidget(self.word_select_label)
		alt_propositions.addWidget(self.word_select)

		self.solver_screen.addWidget(self.label_explanation)
		self.solver_screen.addLayout(self.spots)
		self.solver_screen.addLayout(alt_propositions)
		self.solver_screen.addWidget(self.button_validate)
		self.solver_screen.addWidget(self.label_carousel)
		self.solver_screen.addWidget(self.label_colors)
		self.solver_screen.addLayout(self.proposition_layout)
		self.solver_screen_widget = QWidget()
		self.solver_screen_widget.setLayout(self.solver_screen)
		
		self.color_map = {
			LetterState.INCORRECT: None,
			LetterState.CORRECT: "rgb(44, 117, 5)",
			LetterState.MISPLACED: "rgb(166, 120, 28)"
		}
		
		# Final setup
		self.main_widget = QStackedWidget()
		self.main_widget.addWidget(self.main_menu_widget)
		self.main_widget.addWidget(self.solver_screen_widget)
		self.setCentralWidget(self.main_widget)

	def buttonsToPattern(self):
		pattern_map = {
			LetterState.INCORRECT: "-",
			LetterState.CORRECT: "o",
			LetterState.MISPLACED: "~"
		}

		buttons = self.letter_buttons[:self.letter_count]
		return "".join(list(pattern_map[letter.state] for letter in buttons))

	def update_button(self, button: LetterButton):
		color = self.color_map[button.state]
		color_stylesheet = f"background-color: {color};" if color is not None else ""

		button.setStyleSheet("QPushButton"
						"{"
						f"{color_stylesheet}"
						"font-size: 64px;"
						"}"
						)

	def set_primary_proposition(self, with_text: str):
		self.current_guess = with_text

		for letter, button in zip(with_text, self.letter_buttons):
			button.setText(letter.upper())

			# Remove yellow, only green is known to stay
			if button.state == LetterState.MISPLACED:
				button.state = LetterState.INCORRECT

			self.update_button(button)

	def set_propositions(self, word_list: List[str]):
		self.set_primary_proposition(word_list[0])

		self.word_select.clear()
		for word in self.solver.words:
			self.word_select.addItem(word)

	def open_main_screen(self):
		self.main_widget.setCurrentWidget(self.main_menu_widget)

	def open_solver_from_main_params(self, letter_count: int):
		dict_paths = self.known_dictionaries[self.language_select.currentText()]
		solver = wordeule.Dictionary(**dict_paths)

		first_letter = self.first_letter_select.currentText()
		if first_letter != "":
			solver.filter_with_pattern(
				first_letter.lower() + "_" * (letter_count - 1),
				"o" + "_" * (letter_count - 1))
		else:
			solver.filter_with_pattern("_" * letter_count, "_" * letter_count)

		self.open_solver(
			solver=solver,
			letter_count=letter_count)

	def open_solver(self, solver: wordeule.Dictionary, letter_count: int):
		self.solver = solver
		self.letter_count = letter_count

		self.solver.filter_with_pattern("_" * letter_count, "_" * letter_count)
		self.set_propositions(self.solver.words)

		while self.spots.count() > 0:
			self.spots.removeWidget(self.spots.itemAt(0).widget())

		for letter in self.letter_buttons[:self.letter_count]:
			letter.show()
			letter.state = LetterState.INCORRECT
			self.update_button(letter)
			self.spots.addWidget(letter)
		
		for letter in self.letter_buttons[self.letter_count:]:
			letter.hide()

		self.main_widget.setCurrentWidget(self.solver_screen_widget)
	
	def validate_word(self):
		pattern = self.buttonsToPattern()

		if pattern.count("o") == len(pattern):
			msg = QMessageBox()
			msg.setText("Le mot a été trouvé 😀\nRetour au menu principal.")
			msg.exec()
			self.open_main_screen()
			return

		self.solver.filter_with_pattern(self.current_guess, pattern)

		if len(self.solver.words) == 0:
			msg = QMessageBox()
			msg.setText(""" \
Le mot n'a pas été trouvé...
Peut-être vous êtes-vous trompé sur un mot, ou la solution n'est pas dans notre dictionnaire.
Retour au menu principal.
""")
			msg.exec()
			self.open_main_screen()
			return

		self.set_propositions(self.solver.words)
		# FIXME: handle the case where there is no word left
		
	def cycle_letter_button(self):
		button = cast(LetterButton, self.sender())
		button.state = LetterState((button.state.value + 1) % 3)		
		self.update_button(button)

def run_ui():
	app = QApplication([])

	window = MainWindow()
	window.show()

	app.exec()
