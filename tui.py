import wordeule
import argparse
from ui import run_ui

def wordeule_tui():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="mode")
    
    tui_parser = subparsers.add_parser("tui")
    tui_parser.add_argument("--dict-file", default="./assets/fr_FR.dic")
    tui_parser.add_argument("--freq-file")
    
    gui_parser = subparsers.add_parser("gui")

    args = parser.parse_args()

    if args.mode == "tui":
        solver = wordeule.Dictionary(args.dict_file, args.freq_file)
        run_tui(solver)
    else:
        run_ui()

def run_tui(solver: wordeule.Dictionary):
    letter_count = int(input("Nombre de lettres: "))
    solver.filter_by_length(letter_count)

    while True:
        guess   = input("Entrez un mot:     ")
        pattern = input("Entrez un pattern: ")
        
        solver.filter_with_pattern(guess, pattern)

        print(f"Possibilités: {solver.words}")
