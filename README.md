# Wordeule

This is a solver for the popular Wordle game, which can also solve similar games such as Motus, etc.

# Installation

Be sure to have python3 installed on your machine:

```bash
# for ubuntu / debian
sudo apt install python3
```

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the requirements.
It is recommended to use a virtual environnement to install this application.

```bash
cd resolveur

pip3 install -r requirements.txt
```
# Tests

You can check if the application is correctly installed when running the test script with the command line below:
```bash
python3 tests.py
```

# Usage

You can run this application with the command line below:
```bash
python3 wordeule.py
```


# Deployment
You can build a windows executable with pyinstaller:
```bash
pyinstaller .\wordeule.py --noconsole --onefile --hiddenimport assets --add-data "assets/fr_FR.dic;assets" --add-data "assets/en_US.dic;assets" --add-data "assets/en_US.freq.csv;assets" 
```
