from copy import deepcopy
from collections import defaultdict
import random
from typing import List, Optional
import unittest
import wordeule

class TestWordeule(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.fr_dict = wordeule.Dictionary("fr_FR.dic")
        cls.en_dict = wordeule.Dictionary("en_US.dic", "en_US.freq.csv")
        with open("./assets/wordle-history.txt", "r") as f:
            cls.wordle_history = f.read().splitlines()

    def test_dict_import(self):
        """Check whether words seem to be imported correctly"""
        self.assertTrue("zygomorphe" in self.fr_dict.words)
    
    def test_dict_normalization(self):
        """Check whether normalization on dictionaries work"""
        self.assertFalse("éventrer" in self.fr_dict.words)
        self.assertTrue("eventrer" in self.fr_dict.words)

    def test_length_filtering(self):
        """Check whether basic length filtering is effective"""
        dict = deepcopy(self.fr_dict)
        dict.filter_by_length(6)
        self.assertTrue("tomate" in dict.words)
        self.assertFalse("chien" in dict.words)
    
    def test_dedup(self):
        dict = deepcopy(self.fr_dict)
        dict.filter_with_pattern("zinzin", "oooooo")
        self.assertTrue(len(dict.words) == 1)

    def test_basic(self):
        """Basic full test for the solver algorithm"""
        dict = deepcopy(self.fr_dict)
        dict.filter_with_pattern("garage", "oo--~~")
        self.assertTrue("gadget" in dict.words)
        self.assertTrue(len(dict.words) < 10)
    
    def test_ligule_bug(self):
        """Check an edge-case where a word would not be filtered out"""
        dict = deepcopy(self.fr_dict)
        dict.filter_with_pattern("ligule", "o-oo-o")
        self.assertFalse("ligule" in dict.words)

    def test_newline_import_bug(self):
        self.assertTrue("avian" in self.en_dict.words)

    # def test_vowel_count(self):
    #     """Test vowel counting"""
    #     self.assertEqual(wordeule.vowels_matched("baguette"), 3)

    def test_pattern(self):
        self.assertEqual(wordeule.get_pattern_for_word("abcd", "abcd"), "oooo")
        self.assertEqual(wordeule.get_pattern_for_word("aab", "abb"), "o-o")
        self.assertEqual(wordeule.get_pattern_for_word("aba", "bab"), "~~-")

    def solve_known(self, dict: wordeule.Dictionary, to_guess: str) -> Optional[List[str]]:
        guess_list = []

        i = 0
        while len(dict.words) > 0:
            if i == 0:
                guess = "crane"
            else:
                guess = dict.words[0]
                
            guess_list.append(guess)

            if guess == to_guess:
                return guess_list

            pattern = wordeule.get_pattern_for_word(guess, to_guess)

            #print(f"{guess} vs {to_guess} gives pattern {pattern}")

            dict.filter_with_pattern(guess, pattern)

            i += 1

        return None

    def test_performance(self):
        perf_freq = defaultdict(lambda: 0.0)
        slice = random.sample(self.wordle_history, 500)

        base_dict = deepcopy(self.en_dict)
        base_dict.filter_with_pattern("_" * 5, "_" * 5)

        for word in slice:
            cloned_dict = deepcopy(base_dict)
            seq = self.solve_known(cloned_dict, word)
            if seq is not None:
                perf_freq[len(seq)] += 1 / len(slice)
            print(f"{word} guessed with tries {seq}")

        avg_length = sum(k * v for k, v in perf_freq.items())
        print(avg_length)
        
        # import matplotlib.pyplot as plt
        # plt.bar(perf_freq.keys(), perf_freq.values())
        # plt.savefig("letter-freq-score.png")

if __name__ == '__main__':
    unittest.main()